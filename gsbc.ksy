---
meta:
  id: gsbc
  endian: le
  encoding: ASCII

seq:
  - id: ax25_frame
    type: ax25_frame
    doc-ref: 'https://www.tapr.org/pub_ax25.html'

types:
  ax25_frame:
    seq:
      - id: ax25_header
        type: ax25_header
      - id: payload
        type:
          switch-on: ax25_header.ctl & 0x13
          cases:
            0x03: ui_frame
            0x13: ui_frame
            0x00: i_frame
            0x02: i_frame
            0x10: i_frame
            0x12: i_frame
            # 0x11: s_frame

  ax25_header:
    seq:
      - id: dest_callsign_raw
        type: callsign_raw
      - id: dest_ssid_raw
        type: ssid_mask
      - id: src_callsign_raw
        type: callsign_raw
      - id: src_ssid_raw
        type: ssid_mask
      - id: repeater
        type: repeater
        if: (src_ssid_raw.ssid_mask & 0x01) == 0
        doc: 'Repeater flag is set!'
      - id: ctl
        type: u1

  repeater:
    seq:
      - id: rpt_instance
        type: repeaters
        repeat: until
        repeat-until: ((_.rpt_ssid_raw.ssid_mask & 0x1) == 0x1)
        doc: 'Repeat until no repeater flag is set!'

  repeaters:
    seq:
      - id: rpt_callsign_raw
        type: callsign_raw
      - id: rpt_ssid_raw
        type: ssid_mask

  callsign_raw:
    seq:
      - id: callsign_ror
        process: ror(1)
        size: 6
        type: callsign

  callsign:
    seq:
      - id: callsign
        type: str
        encoding: ASCII
        size: 6

  ssid_mask:
    seq:
      - id: ssid_mask
        type: u1
    instances:
      ssid:
        value: (ssid_mask & 0x0f) >> 1

  i_frame:
    seq:
      - id: pid
        type: u1
      - id: ax25_info
        type: ax25_info_data
        size-eos: true

  ui_frame:
    seq:
      - id: pid
        type: u1
      - id: ax25_info
        type: ax25_info_data
        size-eos: true

  ax25_info_data:
    seq:
      - id: data_monitor
        type: gsbc_header

  gsbc_header:
    seq:
      - id: payload_id
        type: u1
      - id: payload_type
        type:
          switch-on: payload_id
          cases:
            0x40: weather

  weather:
    seq:
      - id: day
        type: str
        size: 2
        -orig-id: day

      - id: hour
        type: str
        size: 2
        -orig-id: hour

      - id: minute
        type: str
        terminator: 0x7a
        -orig-id: minite

      - id: lat_deg
        type: str
        size: 2
        -orig-id: lat_deg
      - id: lat_min
        type: str
        size: 2
        -orig-id: lat_min
      - id: lat_dot
        type: u1
      - id: lat_dmin
        type: str
        size: 2
        -orig-id: lat_dmin
      - id: lat_hem
        type: str
        size: 1
        -orig-id: lat_hem
      - id: latlon
        type: u1

      - id: lon_deg
        type: str
        size: 3
        -orig-id: lon_deg
      - id: lon_min
        type: str
        size: 2
        -orig-id: lon_min
      - id: lon_dot
        type: u1
      - id: lon_dmin
        type: str
        size: 2
        -orig-id: lon_dmin
      - id: lon_hem
        type: str
        size: 1
        -orig-id: lon_hem

      - id: wind_id
        type: u1
      - id: wind
        type: str
        size: 3
      - id: windwinddir
        type: u1
      - id: wind_dir
        type: str
        size: 3
      - id: gust_id
        type: u1
      - id: gust
        type: str
        size: 3
      - id: bme_temp_id
        type: u1
      - id: bme_temp
        type: str
        size: 3
        -orig-id: bme_temp

      - id: bme_humid_id
        type: u1
      - id: bme_humid
        type: str
        size: 2
        -orig-id: bme_humid

      - id: bme_pres_id
        type: u1
      - id: bme_pres
        type: str
        size: 5
        -orig-id: bme_pres
      - id: station_id
        type: u1
      - id: station
        type: str
        size: 6
      - id: gps_alt
        type: str
        size: 5
      - id: pressure_altitude
        type: str
        size: 5
      - id: flight_phase
        type: str
        size: 1
      - id: vertical_velocity
        type: str
        size: 5
      - id: system_state
        type: str
        size: 2
      - id: batt_milivolt
        type: str
        size: 4
      - id: atmo_internal_temp
        type: str
        size: 4
      - id: gas_co
        type: str
        size: 5
      - id: muon_level
        type: str
        size: 4
      - id: muon_count
        type: str
        size: 5
    instances:
      timestamp:
        value: >-
          (
            (day.to_i * 24 * 60 * 60) +
            (hour.to_i * 60 * 60) +
            (minute.to_i * 60)
          )
      packet_bme_pressure:
        value: bme_pres.to_i
      packet_bme_temperature:
        value: bme_temp.to_i
      packet_bme_humidity:
        value: bme_humid.to_i
      packet_atmo_temperature:
        value: atmo_internal_temp.to_i
      packet_muon_level:
        value: muon_level.to_i
      packet_muon_count:
        value: muon_count.to_i
      packet_gps_fix:
        value: '(system_state.to_i & 16 == 16) ? 1 : 0'
      packet_gps_alt:
        value: gps_alt.to_i
      packet_gps_lat:
        value: '(lat_deg.to_i + (lat_min.to_i / 60.0) + (lat_dmin.to_i / 6000.0)) * ((lat_hem == "N") ? 1 : -1)'
      packet_gps_lon:
        value: '(lon_deg.to_i + (lon_min.to_i / 60.0) + (lon_dmin.to_i / 6000.0)) * ((lon_hem == "N") ? 1 : -1)'
      packet_nav_pressure_altitude:
        value: pressure_altitude.to_i
      packet_nav_vertical_velocity:
        value: vertical_velocity.to_i
      packet_system_flight_phase:
        value: flight_phase.to_i
      packet_system_power_camera:
        value: '(system_state.to_i & 1 == 1) ? 1 : 0'
      packet_system_power_pyro:
        value: '(system_state.to_i & 2 == 2) ? 1 : 0'
      packet_system_power_buzzer:
        value: '(system_state.to_i & 8 == 8) ? 1 : 0'
      packet_system_power_mics:
        value: '(system_state.to_i & 4 == 4) ? 1 : 0'
      packet_windspeed:
        value: wind.to_i
      packet_winddir:
        value: wind_dir
      packet_gust:
        value: gust.to_i
      packet_batt_milivolt:
        value: batt_milivolt.to_i
