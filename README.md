# HAB Base station software

This is the base station reception and vusalization software.

It will demodulate data from an SDR, and display it using Grafana through InfluxDB

## Intallation

KaiTai and InfuxDB connector

`pip3 install kaitaistruct influxdb`

Docker

`sudo apt install docker-ce`

GNU Radio

`sudo apt install gnuradio`


gr-satnogs

Follow instructions at https://gitlab.com/librespacefoundation/satnogs/gr-satnogs

## Run

`./start.sh`

Open http://localhost:3000/

Import HAB.json in grapfana