#!/usr/bin/python
from gsbc import Gsbc
import sys
import json
import socket
from influxdb import InfluxDBClient
import threading
from subprocess import call
from pprint import pprint

UDP_IP = "127.0.0.1"
GRC_UDP_PORT =5022
INFLUX_HOST = 'localhost'
INFLUX_PORT = 8086
INFLUX_USER = 'root'
INFLUX_PASSWORD = 'root'
INFLUX_DB = 'gsbc'

def gsbc2influx(packet):
    return [
    {
        "measurement": "GSBC",
        "tags": {
            "VehicleID": "PQ",
            "version": "1.0"
        },
        # "time": "2009-11-10T23:00:00Z",
        "fields": {
            "BatVolt": packet.packet_batt_milivolt,
            "Pressure": packet.packet_bme_pressure,
            'Temperature-BME': packet.packet_bme_temperature,
            'Humidity-BME': packet.packet_bme_humidity,
            'Temperature-Atmo': packet.packet_atmo_temperature,
            'Muon-level': packet.packet_muon_level,
            'Muon-count': packet.packet_muon_count,
            'GPS-Fix': packet.packet_gps_fix,
            'GPS-Alt': packet.packet_gps_alt,
            'GPS-Lat': packet.packet_gps_lat,
            'GPS-Lon': packet.packet_gps_lon,
            'Pressure-Altitude': packet.packet_nav_pressure_altitude,
            'Vertical-Velocity': packet.packet_nav_vertical_velocity,
            'Flight-Phase': packet.packet_system_flight_phase,
            'Power-Camera': packet.packet_system_power_camera,
            'Power-Pyro': packet.packet_system_power_pyro,
            'Power-Buzzer': packet.packet_system_power_buzzer,
            'Power-MICS': packet.packet_system_power_mics
        }
    }]

#  start gnuradio
def thread_grc():
    call(["python", "pq9.py"])
processThread = threading.Thread(target=thread_grc)
processThread.start()

client = InfluxDBClient('localhost', 8086, 'root', 'root', INFLUX_DB)
# client.drop_database(INFLUX_DB)
client.create_database(INFLUX_DB)

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, GRC_UDP_PORT))
print ("Listening on UDP port ", GRC_UDP_PORT)

while True:
    data, addr = sock.recvfrom(200)
    # print (data)
    # print >>sys.stderr, 'received %s bytes from %s' % (len(data), addr)
    try:
        gsbc_data = Gsbc.from_bytes(data)
        # print "Packet from Vehicle: ", gsbc_data.Weather.station
        influx_records = gsbc2influx(gsbc_data.ax25_frame.payload.ax25_info.data_monitor.payload_type)
        # pprint (influx_records)
        try:
            client.write_points(influx_records)
        except:
            pprint ('db error', sys.exc_info())
    except Exception as e:
        print ('Bad packet')
        print (e)
